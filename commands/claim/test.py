import datetime
import unittest
from unittest.mock import Mock

from commands.claim.command_decorator import check_claim_datetime


class TestAppeal(unittest.TestCase):

    def test_check_appeal_datetime(self):
        launchpad = Mock()
        appeal_id = 0
        user_id = 1

        data = datetime.datetime.now() - datetime.timedelta(days=1)

        launchpad.get_claim_datetime.return_value = data
        ok, appeal_time = check_claim_datetime(launchpad, appeal_id, user_id)

        self.assertTrue(ok)
        self.assertIsNone(appeal_time)

        data = None

        launchpad.get_claim_datetime.return_value = data
        ok, appeal_time = check_claim_datetime(launchpad, appeal_id, user_id)

        self.assertTrue(ok)
        self.assertIsNone(appeal_time)

        data = datetime.datetime.now() - datetime.timedelta(hours=12)

        launchpad.get_claim_datetime.return_value = data
        ok, appeal_time = check_claim_datetime(launchpad, appeal_id, user_id)

        self.assertFalse(ok)
        self.assertIsNotNone(appeal_time)

        pass


    pass
