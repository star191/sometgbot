import datetime

import math
from telegram import Update, Message, User
from telegram.ext import CallbackContext

import middlewares
from bot import Launchpad
from middlewares import MessageMiddleware


def claim_decorator(launchpad: Launchpad):
    debug_private_chat_off = True

    # reply to current chat
    def claim(update: Update, context: CallbackContext):
        print('/claim')

        message: Message
        reply: Message
        sender: User

        message = update.message
        sender = message.from_user
        if sender is None:
            print('sender is None')
            return

        # send 'Yes, My Master'
        # send_yes_My_Master(update, context)

        # reply message
        reply = message.reply_to_message

        replier: User

        if reply is None:
            print('Reply is None')
            message.reply_text(f'Use with reply')
            return
        else:
            replier = reply.from_user

        if replier is None:
            print('Replier is None')
            return

        # check chat type (must be group or supergroup)
        if not debug_private_chat_off:
            chat = message.chat
            if chat.type != chat.GROUP and chat.type != chat.SUPERGROUP:
                print('chat is not group')
                message.reply_text(f'This chat is not a group or supergroup')
                print(f'chat type: {chat.type}')
                return

        claimer_id = sender.id

        # check if user already claim
        ok, claim_time_delta = check_claim_datetime(launchpad, claimer_id, replier.id)
        if not ok:
            print('fail by date_check')
            claim_time = math.ceil(claim_time_delta.total_seconds() / 3600)
            message.reply_text(f'You already claim today. Try through: {claim_time} hours')
            return

        add_claim_to_db(launchpad, claimer_id, replier.id)

        # create MessageMiddleware; handle the user answer with claim message
        launchpad.add_message_middleware(MessageMiddleware(callback=answer_middleware_decorator(sender_id=sender.id, replier=replier),
                                                           m_type=middlewares.ONCE))

        message.reply_text(f'Your next (one) message will be forwarded to the administration.')

    return claim
    pass


def answer_middleware_decorator(sender_id: int, replier: User):

    def answer_middleware(launchpad: Launchpad, update: Update, context: CallbackContext):
        print('handle_answer_middleware()')

        message: Message
        sender: User

        message = update.message
        sender = message.from_user
        if sender is None:
            print('sender is None')
            return

        # check that it is the same user
        if sender_id != sender.id:
            return

        claim_text = message.text

        inform_owner(launchpad, sender, replier, claim_text)

        pass

    return answer_middleware


def inform_owner(launchpad, sender, replier, appeal_text):

    message_text = f'{sender.name} claim to {replier.name}\nClaim text:\n{appeal_text}'

    for owner_id in launchpad.owners:
        launchpad.updater.bot.send_message(chat_id=owner_id, text=message_text)
    pass


def add_claim_to_db(launchpad, claimer_id, user_id):
    print('add_claim_to_db()')
    launchpad.add_claim(claimer_id, user_id)
    launchpad.config.save_claims()
    pass


def check_claim_datetime(launchpad: Launchpad, claimer_id: int, user_id: int) -> (bool, datetime):
    last_claim_time = launchpad.get_claim_datetime(claimer_id, user_id)

    # if user didn't claim
    if last_claim_time is None:
        return True, None

    # if user did claim greater than 1 day
    today = datetime.datetime.now()
    delta = datetime.timedelta(days=1)
    if gte_delta(today, last_claim_time, delta):
        return True, None

    # if less
    claim_time = delta - (today - last_claim_time)

    return False, claim_time


def gte_delta(future: datetime, past: datetime, delta: datetime.timedelta) -> bool:
    if future - past >= delta:
        return True
    else:
        return False
    pass
