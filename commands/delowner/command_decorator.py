from telegram import Update, Message, User
from telegram.error import BadRequest
from telegram.ext import CallbackContext

from bot import Launchpad
from commands import send_yes_My_Master
from permissions import send_not_permissions


def delowner_decorator(launchpad: Launchpad):

    # reply to current chat
    def delowner(update: Update, context: CallbackContext):
        print('/delowner')

        message: Message
        reply: Message
        sender: User

        message = update.message
        sender = message.from_user
        if sender is None:
            return

        # check permissions
        if not launchpad.check_permission(sender.id):
            send_not_permissions(message, sender)
            return

        # send 'Yes, My Master'
        send_yes_My_Master(update, context)

        # reply message
        reply = message.reply_to_message

        try:
            replier: User

            if reply is None:
                print('Reply is None')
                message.reply_text(f'Use with reply')
                return
            else:
                replier = reply.from_user

            if replier is None:
                print('Replier is None')
                return
            else:
                ok, reason = del_user_from_owners(launchpad, replier.id)
                if not ok and reason == 'not_exist':
                    message_text = f'{replier.username} is not My Master'
                    context.bot.send_message(message.chat_id, message_text)
                    return

                message_text = f'{replier.username} now is not Bot Owner'
                context.bot.send_message(message.chat_id, message_text)
                message_text = f'{replier.name}\n' \
                    f'All those moments will be lost in time, '\
                    f'like tears in rain. '\
                    f'Time to say goodbye.'
                context.bot.send_message(message.chat_id, message_text)

            # raise_init_conversation_error_debug()

        except BadRequest as e:
            print(e.message)

    return delowner


def del_user_from_owners(lp: Launchpad, user_id) -> (bool, str):
    return lp.del_owner(user_id)
