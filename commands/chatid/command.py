from telegram import Update, Message, User
from telegram.ext import CallbackContext


def chatid(update: Update, context: CallbackContext):
    print('/chatid')

    message: Message
    sender: User

    message = update.message
    sender = message.from_user
    if sender is not None:
        sender.send_message(message.chat_id)
        print(f'{message.chat_id}')

    pass
