import threading
import time
from typing import List

from telegram import Update, Message, User, ReplyKeyboardMarkup, ReplyKeyboardRemove
from telegram.ext import CallbackContext

import middlewares
from bot import Launchpad
from resources import Strings
from resources import getLocalString

DEBUG_MODE = False
DEBUG_PRIVATE_CHAT_OFF = False


class DataToAdmins:
    from_chat_id: int

    # List of Tuples
    # Tuple: (message_id, text before forward message)
    messages: List[tuple]

    def __init__(self):
        self.messages = []
        pass

    def add_message(self, message_id: int, text_before=None):
        tpl = (message_id, text_before)
        self.messages.append(tpl)
        pass

    pass


def join_decorator(launchpad: Launchpad):

    def join(update: Update, context: CallbackContext):
        print('/join')

        message: Message
        sender: User
        args: [str]

        message = update.message
        sender = message.from_user
        if sender is None:
            print('sender is None')
            return

        # Not in private chat
        if not DEBUG_PRIVATE_CHAT_OFF:
            if message.chat.type != message.chat.PRIVATE:
                print('Chat type is not private')
                send_not_in_private_chat(message)
                return

        ok = user_was_already_in_main_group(launchpad, sender.id)
        # Main Group is None
        if ok is None:
            print('Main Group is None')
            return

        # user is in Main Group
        if ok:
            print('User in Main Group')
            send_welcome_again(launchpad, message)
            return

        start_join_conversation(launchpad, sender, message)

        pass

    return join


def user_was_already_in_main_group(launchpad: Launchpad, user_id):
    return launchpad.config.check_user_in_main_group_history(user_id)
    pass


def send_welcome_again(launchpad: Launchpad, message: Message):
    link = launchpad.get_invite_link_to_group()
    message_text = f'{getLocalString(Strings.join_Welcome_back)}\n{link}'
    message.reply_text(message_text)
    pass


def send_not_in_private_chat(message: Message):
    message_text = getLocalString(Strings.join_Use_the_command_only_in_private_chat)
    message.reply_text(message_text)
    pass


# entry point of conversation dialog
def start_join_conversation(launchpad: Launchpad, sender, message: Message):

    # fill data
    data = DataToAdmins()
    data.from_chat_id = message.chat_id

    # add conversation middlewares
    if DEBUG_MODE:
        add_conversation_middleware(launchpad, sender.id, test_stage_conversation_decorator(data=data))
        message_text = getLocalString(Strings.join_Test_conversation)
        message.reply_text(message_text)
    else:
        send_conversation_help(message)
        stage_one_conversation_question(message)
        add_conversation_middleware(launchpad, sender.id, one_stage_conversation_decorator(data=data))
    pass


def add_conversation_middleware(launchpad: Launchpad, user_id, callback):
    ware = middlewares.ConversationMiddleware(conversation_user_id=user_id, callback=callback)
    launchpad.messageMiddlewareManager.add_ware(ware)
    pass


def send_data_to_admins(launchpad: Launchpad, message: Message, context: CallbackContext, data: DataToAdmins):
    print('send_data_to_admins()')

    if launchpad.owners is None or len(launchpad.owners) == 0:
        send_no_owners(message)
        return

    send_args = (launchpad, context, data)
    send_thread = threading.Thread(target=__send_data_to_admins, args=send_args)
    # __send_data_to_admins(launchpad, context, data)
    send_thread.start()

    pass


def __send_data_to_admins(launchpad: Launchpad, context: CallbackContext, data: DataToAdmins):
    notifs = False
    count = 0

    first_message_text = getLocalString(Strings.join_Novice_wants_to_join_chat)

    for owner_id in launchpad.owners:

        # sending data to owners
        if first_message_text != '':
            context.bot.send_message(chat_id=owner_id, text=first_message_text)

        for tpl in data.messages:
            # send message before forwarded message
            if tpl[1] is not None and len(tpl[1]) != 0:
                context.bot.send_message(chat_id=owner_id, text=tpl[1])
                # delay for antispam
                count = __delay_sending_and_increment_the_count(count)
                pass

            # send forwarded message
            context.bot.forward_message(
                chat_id=owner_id,
                from_chat_id=data.from_chat_id,
                message_id=tpl[0],
                disable_notification=notifs
            )

            # delay for antispam
            count = __delay_sending_and_increment_the_count(count)
    pass


def __delay_sending_and_increment_the_count(count: int) -> int:
    # default message value for delay
    delay_count = 20

    # time in seconds
    delay_time = 1

    if count >= delay_count:
        time.sleep(delay_time)
        count = 0
        return count

    # increment
    count = count + 1
    return count


def is_cancel(update: Update, context: CallbackContext) -> bool:
    message: Message
    message = update.message

    # cancel_tokens = [getLocalString(Strings.join_CANCEL), getLocalString(Strings.join_STOP)]
    cancel_tokens = getLocalString(Strings.join_CANCEL_STOP)

    if message.text is None:
        return False

    if any(token in message.text.lower() for token in cancel_tokens):
        return True

    return False
    pass


def check_YES_NO(message: Message):
    tokens = getLocalString(Strings.join_YES_NO)
    if any(token in message.text.lower() for token in tokens):
        return True
    return False


def send_wrong_answer(message: Message, message_text=None):
    if message_text is None:
        message_text = getLocalString(Strings.join_Wrong_answer)
    message.reply_text(message_text)
    pass


def send_no_owners(message: Message, message_text=None):
    if message_text is None:
        message_text = getLocalString(Strings.join_Error_Owners_not_set)
    message.reply_text(message_text)
    pass


def check_photo(message: Message):
    if message.photo is None or len(message.photo) <= 0:
        return False
    return True


def cancel_reply(update: Update):
    update.message.reply_text(getLocalString(Strings.join_Stop_conversation),
                              reply_markup=ReplyKeyboardRemove())
    pass


def stage_one_conversation_question(message: Message):
    message_text = getLocalString(Strings.join_Question_1)
    reply_keyboard = [[getLocalString(Strings.join_YES), getLocalString(Strings.join_NO)]]
    message.reply_text(message_text, reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    pass

def send_conversation_help(message: Message):
    message_text = getLocalString(Strings.join_Conversation_Help)
    message.reply_text(message_text)
    pass


def test_stage_conversation_decorator(data: DataToAdmins):
    def callback(launchpad: Launchpad, update: Update, context: CallbackContext) -> bool:
        message: Message
        message = update.message

        # set to data
        data.add_message(message_id=message.message_id, text_before="Text before:")

        send_data_to_admins(launchpad, message, context, data)
        message_text = 'Test Stop'
        message.reply_text(message_text, reply_markup=ReplyKeyboardRemove())

        return True
        pass
    return callback


def one_stage_conversation_decorator(data: DataToAdmins):
    def callback(launchpad: Launchpad, update: Update, context: CallbackContext) -> bool:
        print('one_stage_conversation_callback()')

        # exit from conversation
        if is_cancel(update, context):
            cancel_reply(update)
            return True

        message: Message
        message = update.message

        # check answer
        if not check_YES_NO(message):
            send_wrong_answer(message)
            return False

        # set to data
        data.add_message(message_id=message.message_id, text_before=getLocalString(Strings.join_Question_1))

        # next step
        message_text = getLocalString(Strings.join_Question_2)
        reply_keyboard = [[getLocalString(Strings.join_YES), getLocalString(Strings.join_NO)]]
        message.reply_text(message_text, reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
        add_conversation_middleware(launchpad, message.from_user.id, two_stage_conversation_decorator(data=data))

        return True
        pass
    return callback


def two_stage_conversation_decorator(data: DataToAdmins):
    def callback(launchpad: Launchpad, update: Update, context: CallbackContext) -> bool:
        print('two_stage_conversation_callback()')

        # exit from conversation
        if is_cancel(update, context):
            cancel_reply(update)
            return True

        message: Message
        message = update.message

        # check answer
        if not check_YES_NO(message):
            send_wrong_answer(message)
            return False

        # set to data
        data.add_message(message_id=message.message_id, text_before=getLocalString(Strings.join_Question_2))

        # next step
        message_text = getLocalString(Strings.join_Question_3)
        message.reply_text(message_text, reply_markup=ReplyKeyboardRemove())
        add_conversation_middleware(launchpad, message.from_user.id, three_stage_conversation_decorator(data=data))

        return True
        pass
    return callback


def three_stage_conversation_decorator(data: DataToAdmins):
    def callback(launchpad: Launchpad, update: Update, context: CallbackContext) -> bool:
        print('three_stage_conversation_callback()')

        # exit from conversation
        if is_cancel(update, context):
            cancel_reply(update)
            return True

        message: Message
        message = update.message

        # set to data
        data.add_message(message_id=message.message_id, text_before=getLocalString(Strings.join_Question_3))

        # next step
        message_text = getLocalString(Strings.join_Question_4)
        message.reply_text(message_text, reply_markup=ReplyKeyboardRemove())
        add_conversation_middleware(launchpad, message.from_user.id, four_stage_conversation_decorator(data=data))

        return True
        pass
    return callback


def four_stage_conversation_decorator(data: DataToAdmins):
    def callback(launchpad: Launchpad, update: Update, context: CallbackContext) -> bool:
        print('four_stage_conversation_callback()')

        # exit from conversation
        if is_cancel(update, context):
            cancel_reply(update)
            return True

        message: Message
        message = update.message

        # set to data
        data.add_message(message_id=message.message_id, text_before=getLocalString(Strings.join_Question_4))

        # next step
        message_text = getLocalString(Strings.join_Question_5)
        message.reply_text(message_text, reply_markup=ReplyKeyboardRemove())
        add_conversation_middleware(launchpad, message.from_user.id, five_stage_conversation_decorator(data=data))

        return True
        pass
    return callback


def five_stage_conversation_decorator(data: DataToAdmins):
    def callback(launchpad: Launchpad, update: Update, context: CallbackContext) -> bool:
        print('five_stage_conversation_callback()')

        # exit from conversation
        if is_cancel(update, context):
            cancel_reply(update)
            return True

        message: Message
        message = update.message

        # set to data
        data.add_message(message_id=message.message_id, text_before=getLocalString(Strings.join_Question_5))

        # next step
        message_text = getLocalString(Strings.join_Question_6)
        message.reply_text(message_text, reply_markup=ReplyKeyboardRemove())
        add_conversation_middleware(launchpad, message.from_user.id, six_stage_conversation_decorator(data=data))

        return True
        pass
    return callback


def six_stage_conversation_decorator(data: DataToAdmins):
    def callback(launchpad: Launchpad, update: Update, context: CallbackContext) -> bool:
        print('six_stage_conversation_callback()')

        # exit from conversation
        if is_cancel(update, context):
            cancel_reply(update)
            return True

        message: Message
        message = update.message

        # check no photo
        if not check_photo(message):
            send_wrong_answer(message, message_text=getLocalString(Strings.join_Send_a_photo))
            return False

        # set to data
        data.add_message(message_id=message.message_id, text_before=getLocalString(Strings.join_Question_7))

        # next step
        message_text = getLocalString(Strings.join_Question_7)
        message.reply_text(message_text, reply_markup=ReplyKeyboardRemove())
        add_conversation_middleware(launchpad, message.from_user.id, seven_stage_conversation_decorator(data=data))
        
        return True
        pass
    return callback


def seven_stage_conversation_decorator(data: DataToAdmins):
    def callback(launchpad: Launchpad, update: Update, context: CallbackContext) -> bool:
        print('seven_stage_conversation_decorator()')

        # exit from conversation
        if is_cancel(update, context):
            cancel_reply(update)
            return True

        message: Message
        message = update.message

        # check no photo
        if not check_photo(message):
            send_wrong_answer(message, message_text=getLocalString(Strings.join_Send_a_photo))
            return False

        # set to data
        data.add_message(message_id=message.message_id, text_before=getLocalString(Strings.join_Question_6))

        # end step
        message_text = getLocalString(Strings.join_end_conversation)
        message.reply_text(message_text, reply_markup=ReplyKeyboardRemove())

        send_data_to_admins(launchpad, message, context, data)

        return True
        pass
    return callback