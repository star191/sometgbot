from telegram import Update, Message, User
from telegram.ext import CallbackContext

from bot import Launchpad


def setlink_decorator(launchpad: Launchpad):

    # reply to current chat
    def setlink(update: Update, context: CallbackContext):
        print('/setlink')

        message: Message
        sender: User
        args: [str]

        message = update.message
        sender = message.from_user
        if sender is None:
            print('sender is None')
            return

        # check permissions
        if not launchpad.check_permission(sender.id):
            from permissions import send_not_permissions
            send_not_permissions(message, sender)
            return

        if context.args is None or len(context.args) <= 0:
            send_no_args_message(message)
            return
        else:
            args = context.args

        link = args[0]
        # validate and set
        if not validate_link(link):
            send_invalid_link(message)
            return

        set_invite_link(launchpad, link)

        send_ok(message)

        pass

    return setlink


def send_ok(message):
    message_text = f'ok'
    message.reply_text(message_text)
    pass


def send_invalid_link(message):
    message_text = f'Invalid invite link'
    message.reply_text(message_text)
    pass


def validate_link(link) -> bool:
    return True


def set_invite_link(launchpad: Launchpad, invite_link):
    launchpad.set_invite_link(invite_link)
    pass


def send_no_args_message(message):
    message_text = f'Input the invite link.\n /setlink <invite_link>'
    message.reply_text(message_text)
    pass
