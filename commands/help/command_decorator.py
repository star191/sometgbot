from telegram import Update, Message, User
from telegram.ext import CallbackContext

from bot import Launchpad
from resources import getLocalString, Strings


def help_decorator(launchpad: Launchpad):

    def help(update: Update, context: CallbackContext):
        print('/help')

        message: Message
        sender: User

        message = update.message
        sender = message.from_user
        if sender is None:
            return

        message_text = getLocalString(Strings.help_Use_the_join)
        sender.send_message(message_text)

        pass

    return help

