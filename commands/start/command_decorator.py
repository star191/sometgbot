from telegram import Update, Message, User
from telegram.ext import CallbackContext

from bot import Launchpad
from resources import getLocalString, Strings


def start_decorator(launchpad: Launchpad):

    def start(update: Update, context: CallbackContext):
        print('/start')

        message: Message
        sender: User

        message = update.message
        sender = message.from_user
        if sender is None:
            return

        if is_banned_in_main_group(launchpad, sender.id):
            message_text = getLocalString(Strings.start_You_are_banned)
            sender.send_message(message_text)
            return
            pass

        message_text = getLocalString(Strings.start_Try_join)
        sender.send_message(message_text)

        pass

    return start


def is_banned_in_main_group(launchpad: Launchpad, user_id) -> bool:
    result = False

    banned_users_id = launchpad.config.main_group['banned_users_ids']

    if user_id in banned_users_id:
        result = True

    return result
