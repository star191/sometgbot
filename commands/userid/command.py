from telegram import Update, Message, User, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.error import Unauthorized, BadRequest
from telegram.ext import CallbackContext


# reply to current chat
def cuserid(update: Update, context: CallbackContext):
    print('/cuserid')

    message: Message
    reply: Message
    sender: User

    message = update.message
    sender = message.from_user
    if sender is None:
        return

    reply = message.reply_to_message

    # send self id
    if reply is None:
        answer1 = f'{sender.name} {sender.full_name}'
        answer2 = f'{sender.id}'
        send_answer(context, message.chat_id, answer1, answer2)

    # send replier id
    else:
        replier: User
        replier = reply.from_user
        if reply is not None:
            answer1 = f'{replier.name} {replier.full_name}'
            answer2 = f'{replier.id}'
            send_answer(context, message.chat_id, answer1, answer2)

    pass


def userid(update: Update, context: CallbackContext):
    print('/userid')

    message: Message
    reply: Message
    sender: User

    message = update.message
    sender = message.from_user
    if sender is None:
        return

    reply = message.reply_to_message

    try:
        chat_id = sender.id

        # send self id
        if reply is None:
            answer1 = f'{sender.name} {sender.full_name}'
            answer2 = f'{sender.id}'
            send_answer(context, chat_id, answer1, answer2)

        # send replier id
        else:
            replier: User
            replier = reply.from_user
            if reply is not None:
                answer1 = f'{replier.name} {replier.full_name}'
                answer2 = f'{replier.id}'
                send_answer(context, chat_id, answer1, answer2)

        # raise_init_conversation_error_debug()

    except Unauthorized as e:
        print(e.message)

        # if bot can't initiate conversation, get /start to user
        handling_error_message = "bot can't initiate conversation with a user"
        if handling_error_message in e.message:
            init_conversation(context, message.chat_id)
            return

    except BadRequest as e:
        print(e.message)

    pass


def raise_init_conversation_error_debug():
    raise Unauthorized("bot can't initiate conversation with a user")


def send_answer(context: CallbackContext, chat_id, answer1: str, answer2: str):
    context.bot.send_message(chat_id, answer1)
    context.bot.send_message(chat_id, answer2)


# init conversation with user
def init_conversation(context: CallbackContext, chat_id):
    method = 'start'
    params = 'init_conversation'
    button_url = f'https://t.me/{context.bot.username}?{method}={params}'
    button_text = 'Handshake'
    message_text = 'I do not know you yet' + '\n' + 'Please talk to me o.o'

    button = [
        InlineKeyboardButton(text=button_text, url=button_url)
    ]

    reply_markup = InlineKeyboardMarkup([
        button
    ])

    context.bot.send_message(chat_id, message_text, reply_markup=reply_markup)

    pass
