from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ReplyKeyboardRemove
from telegram import Update, Message, User
from telegram.ext import CallbackContext

from bot import Launchpad


def test(update: Update, context: CallbackContext):
    print('/test')

    message: Message
    sender: User

    message = update.message
    sender = message.from_user

    # test_init_conversation(context, message.chat_id)

    # del_keyboard(context, message.chat_id)

    test_forward(message, context)

    pass


def test_forward(message: Message, context: CallbackContext):
    chat_id = message.chat_id
    from_chat_id = message.chat_id
    message_id = message.message_id

    context.bot.forward_message(chat_id, from_chat_id, message_id)

    pass


# init conversation with user
def test_init_conversation(context: CallbackContext, chat_id):
    method = 'start'
    button_url = f'https://t.me/{context.bot.username}?{method}'
    button_text = 'Handshake'
    message_text = 'I do not know you yet' + '\n' + 'Please talk to me o.o'

    button = [
        InlineKeyboardButton(text=button_text, url=button_url)
    ]

    reply_markup = InlineKeyboardMarkup([
        button
    ])

    context.bot.send_message(chat_id, message_text, reply_markup=reply_markup)

    pass


# init conversation with user
def test_buttons(context: CallbackContext, chat_id):
    method = 'start'
    url = f'https://t.me/{context.bot.username}?{method}'

    button = [
        InlineKeyboardButton("Click", switch_inline_query='start')
    ]
    reply_markup = InlineKeyboardMarkup([
        button
    ])
    context.bot.send_message(chat_id, 'Test', reply_markup=reply_markup)

    pass


def del_keyboard(context: CallbackContext, chat_id):
    reply_markup = ReplyKeyboardRemove()
    context.bot.send_message(chat_id=chat_id, text="Keyboard delete", reply_markup=reply_markup)


def test_decorator(launchpad: Launchpad):

    def callback(update: Update, context: CallbackContext):
        print('/dtest')
        message: Message
        message = update.message
        message.reply_text("Decorator works!")

    return callback
