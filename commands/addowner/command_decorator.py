from telegram import Update, Message, User
from telegram.error import BadRequest
from telegram.ext import CallbackContext

from bot import Launchpad
from commands import send_yes_My_Master
from permissions import send_not_permissions


def addowner_decorator(launchpad: Launchpad):

    # reply to current chat
    def addowner(update: Update, context: CallbackContext):
        print('/addowner')

        message: Message
        reply: Message
        sender: User

        message = update.message
        sender = message.from_user
        if sender is None:
            return

        # check permissions
        if not launchpad.check_permission(sender.id):
            send_not_permissions(message, sender)
            return

        # send 'Yes, My Master'
        send_yes_My_Master(update, context)

        # reply message
        reply = message.reply_to_message

        try:
            replier: User

            if reply is None:
                print('Reply is None')
                message.reply_text(f'Use with reply')
                return
            else:
                replier = reply.from_user

            if replier is None:
                print('Replier is None')
                return
            else:
                ok, reason = add_user_to_owners(launchpad, replier.id)
                if not ok and reason == 'exist':
                    message_text = f'{replier.name} is already My Master'
                    context.bot.send_message(message.chat_id, message_text)
                    return

                message_text = f'{replier.name} now is Bot Owner'
                context.bot.send_message(message.chat_id, message_text)
                message_text = f'My functionality is in Your hands, My Master'
                context.bot.send_message(message.chat_id, message_text)

            # raise_init_conversation_error_debug()

        except BadRequest as e:
            print(e.message)

    return addowner


def add_user_to_owners(lp: Launchpad, user_id) -> (bool, str):
    return lp.add_owner(user_id)

