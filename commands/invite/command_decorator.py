import telegram
from telegram import Update, Message, User
from telegram.ext import CallbackContext

from bot import Launchpad


def invite_decorator(launchpad: Launchpad):

    # reply to current chat
    def invite(update: Update, context: CallbackContext):
        print('/invite')

        message: Message
        sender: User
        args: [str]

        message = update.message
        sender = message.from_user
        if sender is None:
            print('sender is None')
            return

        # check permissions
        if not launchpad.check_permission(sender.id):
            from permissions import send_not_permissions
            send_not_permissions(message, sender)
            return

        if context.args is None or len(context.args) <= 0:
            send_no_args_message(message)
            return
        else:
            args = context.args

        # get the invite link
        invite_link = get_invite_link(launchpad)
        if invite_link is None:
            send_no_invite_link(message)
            return

        # send invite
        invite_text = get_invite_text(invite_link)
        user_id = args[0]
        try:
            send_invite(context, user_id, invite_text)
        except telegram.TelegramError:
            send_cannot_find_user(message)
            return

        pass

    return invite


def get_invite_text(invite_link) -> str:
    invite_text = f'{invite_link}'
    return invite_text
    pass


def send_no_invite_link(message):
    message_text = f'Invite link not set.\nUse /setlink <invite_link>'
    message.reply_text(message_text)
    pass


def get_invite_link(launchpad: Launchpad):
    invite_link = launchpad.get_invite_link_to_group()
    return invite_link


# raises telegram.TelegramError
def send_invite(context, user_id, invite_text):
    chat: telegram.Chat
    chat = context.bot.get_chat(user_id)

    chat.send_message(invite_text)
    pass


def send_cannot_find_user(message):
    message_text = f'Cannot find the user. Maybe user did not /start conversation with me.'
    message.reply_text(message_text)
    pass


def send_no_args_message(message):
    message_text = f'Input the user id.\nYou can use the /userid command with reply'
    message.reply_text(message_text)
    pass
