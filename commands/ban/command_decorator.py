from telegram import Update, Message, User
from telegram.error import TelegramError
from telegram.ext import CallbackContext

from bot import Launchpad
from permissions import send_not_permissions


def ban_decorator(launchpad: Launchpad):
    debug_not_kick = False

    # reply to current chat
    def ban(update: Update, context: CallbackContext):
        print('/ban')

        message: Message
        reply: Message
        sender: User

        message = update.message
        sender = message.from_user
        if sender is None:
            print('sender is None')
            return

        # check permissions
        if not launchpad.check_permission(sender.id):
            send_not_permissions(message, sender)
            return

        # send 'Yes, My Master'
        # send_yes_My_Master(update, context)

        # reply message
        reply = message.reply_to_message

        replier: User

        if reply is None:
            print('Reply is None')
            message.reply_text(f'Use with reply')
            return
        else:
            replier = reply.from_user

        if replier is None:
            print('Replier is None')
            return

        # check chat type (must be group or supergroup)
        chat = message.chat
        if chat.type != chat.GROUP and chat.type != chat.SUPERGROUP:
            print('chat is not group')
            message.reply_text(f'This chat is not a group or supergroup')
            print(f'chat type: {chat.type}')
            return

        # don't ban yourself
        if replier.id == sender.id:
            message.reply_text(f"Honey, please, do not ban yourself")
            return

        # don't ban the bot
        if launchpad.bot_id == replier.id:
            message.reply_text(f"Are you seriously? I can't ban myself.")
            return

        try:
            ok, result = launchpad.local_ban(chat.id, replier.id, debug_not_kick=debug_not_kick)
            if not ok:
                if result == 'not_added_to_ban_list':
                    print('not_added_to_ban_list')
                    message.reply_text('Cannot add user to ban list')
                    return

        except TelegramError:
            message.reply_text(f'Cannot kick {replier.username}')
            return

        # TODO: send gif with Thor banhummer

        message.reply_text(f'{replier.username} kicked and added to ban list')

    return ban
