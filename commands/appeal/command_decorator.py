import datetime

import math
from telegram import Update, Message, User
from telegram.ext import CallbackContext

import middlewares
from bot import Launchpad
from middlewares import MessageMiddleware


def appeal_decorator(launchpad: Launchpad):

    # reply to current chat
    def appeal(update: Update, context: CallbackContext):
        print('/appeal')

        message: Message
        reply: Message
        sender: User

        message = update.message
        sender = message.from_user
        if sender is None:
            print('sender is None')
            return

        # reply message
        reply = message.reply_to_message

        replier: User

        if reply is None:
            print('Reply is None')
            message.reply_text(f'Use with reply')
            return
        else:
            replier = reply.from_user

        if replier is None:
            print('Replier is None')
            return

        # check chat type (must be private)
        if not check_chat_type_and_send_message(message.chat, message):
            return

        ok = check_appeal_time_and_add_to_db(launchpad, message, sender.id)
        if not ok:
            return

        # create MessageMiddleware; handle the user answer with claim message
        launchpad.add_message_middleware(MessageMiddleware(
            callback=answer_middleware_decorator(sender_id=sender.id),
            m_type=middlewares.ONCE)
        )

        message.reply_text(f'Your next (one) message will be forwarded to the administration.')

    return appeal
    pass


def answer_middleware_decorator(sender_id: int):

    def answer_middleware(launchpad: Launchpad, update: Update, context: CallbackContext):
        print('handle_answer_middleware()')

        message: Message
        sender: User

        message = update.message
        sender = message.from_user
        if sender is None:
            print('sender is None')
            return

        # check that it is the same user
        if sender_id != sender.id:
            return

        claim_text = message.text

        inform_owner(launchpad, sender, claim_text)

        pass

    return answer_middleware


def inform_owner(launchpad, sender, appeal_text):

    message_text = f'{sender.name} appeal\nAppeal text:\n{appeal_text}'

    for owner_id in launchpad.owners:
        launchpad.updater.bot.send_message(chat_id=owner_id, text=message_text)
    pass


def check_appeal_time_and_add_to_db(launchpad, message, user_id) -> bool:
    # check if user already appeal
    ok, appeal_time_delta = check_appeal_datetime(launchpad, user_id)
    if not ok:
        print('fail by date_check')
        appeal_time = math.ceil(appeal_time_delta.total_seconds() / 3600)
        message.reply_text(f'You already appeal today. Try through: {appeal_time} hours')
        return ok

    add_appeal_to_db(launchpad, user_id)

    return ok


def check_chat_type_and_send_message(chat, message) -> bool:
    if chat.type != chat.PRIVATE:
        print('chat is not private')
        message.reply_text(f'This chat is not a private')
        print(f'chat type: {chat.type}')
        return False
    return True


def add_appeal_to_db(launchpad, user_id):
    print('add_appeal_to_db()')
    launchpad.add_appeal(user_id)
    launchpad.config.save_appeals()
    pass


def check_appeal_datetime(launchpad: Launchpad, user_id: int) -> (bool, datetime):
    last_appeal_time = launchpad.get_appeal_datetime(user_id)

    # if user didn't appeal
    if last_appeal_time is None:
        return True, None

    # if user did appeal greater than 1 day
    today = datetime.datetime.now()
    delta = datetime.timedelta(days=1)
    if gte_delta(today, last_appeal_time, delta):
        return True, None

    # if less
    appeal_time = delta - (today - last_appeal_time)

    return False, appeal_time


def gte_delta(future: datetime, past: datetime, delta: datetime.timedelta) -> bool:
    if future - past >= delta:
        return True
    else:
        return False
    pass
