from telegram import Update, Message, User
from telegram.ext import CallbackContext

from bot import Launchpad
from permissions import send_not_permissions


def setmaingroup_decorator(launchpad: Launchpad):
    debug_not_check_group = False

    # reply to current chat
    def setmaingroup(update: Update, context: CallbackContext):
        print('/setmaingroup')

        message: Message
        reply: Message
        sender: User

        message = update.message
        sender = message.from_user
        if sender is None:
            print('sender is None')
            return

        # check permissions
        if not launchpad.check_permission(sender.id):
            send_not_permissions(message, sender)
            return

        # check chat type (must be group or supergroup)
        chat = message.chat
        if not debug_not_check_group and chat.type != chat.GROUP and chat.type != chat.SUPERGROUP:
            print('chat is not group')
            message.reply_text(f'This chat is not a group or supergroup')
            print(f'chat type: {chat.type}')
            return

        if not set_main_group(launchpad, chat.id):
            send_cannot_set_main_group(message)
            return

        send_ok(message)

    return setmaingroup


def set_main_group(launchpad: Launchpad, chat_id) -> bool:
    return launchpad.set_main_group(chat_id)


def send_ok(message: Message):
    message_text = f'The Main Group has been updated.'
    message.reply_text(message_text)
    pass


def send_cannot_set_main_group(message: Message):
    message_text = f'Error. Cannot set this group as the Main Group.'
    message.reply_text(message_text)
    pass

