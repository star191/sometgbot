from telegram import Message, Update, ParseMode
from telegram.ext import CallbackContext


def send_yes_My_Master(update: Update, context: CallbackContext) -> None:
    message: Message
    message = update.message

    message_text = f'*Yes, My Master*'
    context.bot.send_message(chat_id=message.chat_id, text=message_text, parse_mode=ParseMode.MARKDOWN)
    pass