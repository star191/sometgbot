import json
import os
import sys
from typing import List

import locales
from common import Singleton
from group import Group


class Config(metaclass=Singleton):
    # constructor
    def __init__(self, cfg_dir) -> None:

        # directory path to config files, usually './config/
        self._cfg_dir = cfg_dir + '/'

        # dict by config.json file
        self.CONFIG = None

        # token string
        self.TOKEN = None

        # bot owners ids
        self.bot_owners_ids = []  # type: List[int]

        # paths from config.json
        self.paths = {}

        # Channels class objects arrays
        self.groups = []  # type: List[Group]

        # Claims
        self.claims = []  # type: List[dict]

        # Appeals
        self.appeals = []  # type: List[dict]

        # invite link to group
        self.invite_link = None

        # main group
        self.main_group = None

        # logging group
        self.logging_group = None

        # locale
        self.__locale = None

        # load configs and setup them
        self.__load_config()
        self._load_token()
        self.__load_locale()
        self.__load_main_group()
        self.__load_logging_group()
        self._load_groups()
        self.__load_claims()
        self.__load_appeals()

    def __load_logging_group(self):
        key = 'logging_group'
        config_logging_group = self.CONFIG[key]

        try:
            logging_group = {
                'id': config_logging_group['id']
            }
        except KeyError:
            print(f'Cannot read Logging Group param')
            sys.exit(-1)

        self.logging_group = logging_group
        pass

    def __load_locale(self):
        key = 'locale'

        try:
            self.__locale = self.CONFIG[key]
        except KeyError:
            print(f'Cannot load locale: invalid config file')
            self.__locale = locales.Locales.DEFAULT
            return

        if not locales.SetLocale(self.__locale):
            print(f'Cannot load locale: unknown locale')
            return

        pass

    def check_user_in_main_group_history(self, user_id: int):
        if self.main_group is None:
            print('Main Group is None')
            return None
        if user_id in self.main_group['history_users_ids']:
            return True
        else:
            return False
        pass

    def update_main_group(self, main_group):
        self.main_group = main_group
        key = 'main_group'
        config_main_group = self.CONFIG[key]

        try:
            config_main_group['id'] = self.main_group['id']
            config_main_group['users_ids'] = self.main_group['users_ids']
            config_main_group['banned_users_ids'] = self.main_group['banned_users_ids']
            config_main_group['history_users_ids'] = self.main_group['history_users_ids']
        except KeyError:
            print(f'Cannot read Main Group param')

        self.save_config()
        pass

    def update_logging_group(self, logging_group):
        self.logging_group = logging_group
        key = 'logging_group'
        config_logging_group = self.CONFIG[key]

        try:
            config_logging_group['id'] = self.logging_group['id']
        except KeyError:
            print(f'Cannot read Logging Group param')

        self.save_config()
        pass

    def __load_main_group(self):
        key = 'main_group'
        config_main_group = self.CONFIG[key]

        try:
            main_group = {
                'id': config_main_group['id'],
                'users_ids': config_main_group['users_ids'],
                'banned_users_ids': config_main_group['banned_users_ids'],
                'history_users_ids': config_main_group['history_users_ids']
            }
        except KeyError:
            print(f'Cannot read Main Group param')
            sys.exit(-1)

        self.main_group = main_group
        pass

    def save_config(self):
        config_path = self._cfg_dir + 'config.json'
        self.__write_json_file(config_path, self.CONFIG)
        pass

    def save_groups(self):
        self._save_groups()
        pass

    def set_invite_link(self, link: str):
        self.invite_link = link
        self.CONFIG['invite_link'] = link
        self.save_config()
        pass

    def save_appeals(self):
        config_path = self._cfg_dir + self.paths['appeals_file']
        key = 'appeals'
        data = {key: []}

        for entity in self.appeals:
            data[key].append(entity)

        self.__write_json_file(config_path, data)
        pass

    def __load_appeals(self) -> None:
        config_path = self._cfg_dir + self.paths['appeals_file']
        key = 'appeals'

        # create template
        if not os.path.exists(config_path):
            print(f'File {config_path} is not exists')
            print('Create the template')

            appeals = {
                key: []
            }

            self.__write_json_file(config_path, appeals)
            pass

        # read data from json file
        json_file = self.__load_json_file(config_path)

        try:
            # mapping data from dict to object fields
            for val in json_file[key]:
                entity = {
                    'user_id': val['user_id'],
                    'timestamp': val['timestamp']
                }
                self.appeals.append(entity)

        except KeyError:
            print(f'Incorrect file format: {config_path}')
            sys.exit(-1)
        pass

    def save_claims(self):
        config_path = self._cfg_dir + self.paths['claims_file']
        key = 'claims'
        data = {key: []}

        for entity in self.claims:
            data[key].append(entity)

        self.__write_json_file(config_path, data)
        pass

    def __load_claims(self) -> None:
        config_path = self._cfg_dir + self.paths['claims_file']
        key = 'claims'

        # create template
        if not os.path.exists(config_path):
            print(f'File {config_path} is not exists')
            print('Create the template')

            appeals = {
                key: []
            }

            self.__write_json_file(config_path, appeals)
            pass

        # read data from json file
        json_file = self.__load_json_file(config_path)

        try:
            # mapping data from dict to object fields
            for val in json_file[key]:
                entity = {
                    'claimer_id': val['claimer_id'],
                    'user_id': val['user_id'],
                    'timestamp': val['timestamp']
                }
                self.claims.append(entity)

        except KeyError:
            print(f'Incorrect file format: {config_path}')
            sys.exit(-1)
        pass

    # load config file
    def __load_config(self) -> None:
        config_path = self._cfg_dir + 'config.json'
        self.CONFIG = self.__load_json_file(config_path)
        self.__setup_configs()
        pass

    # mapping fields from config
    def __setup_configs(self) -> None:
        self.bot_owners_ids = self.CONFIG['bot_owners_ids']
        self.paths = self.CONFIG['paths']
        pass

    # load token from file
    def _load_token(self) -> None:
        token_path = self._cfg_dir + self.paths['token_file']
        # fix with spaces and newlines, trim all spaces
        self.TOKEN = self.__load_file(token_path).strip()
        pass

    # save channels to groups json config file
    def _save_groups(self) -> None:
        config_path = self._cfg_dir + self.paths['groups_file']
        data = {"groups": []}

        for channel in self.groups:
            data['groups'].append(channel.__dict__)

        self.__write_json_file(config_path, data)

        pass

    # load channels from file
    # create template file if file not exists
    def _load_groups(self) -> None:
        config_path = self._cfg_dir + self.paths['groups_file']

        # create template
        if not os.path.exists(config_path):
            print(f'File {config_path} is not exists')
            print('Create the template')

            channels = {
                "groups": []
            }

            self.__write_json_file(config_path, channels)
            pass

        # read data from json file
        groups_file = self.__load_json_file(config_path)
        try:
            # mapping data from dict to object fields
            for gr in groups_file['groups']:
                group = Group()
                group.id = gr['id']
                group.admins_ids = gr['admins_ids']
                group.users_ids = gr['users_ids']
                group.banned_users_ids = gr['banned_users_ids']
                self.groups.append(group)
        except KeyError:
            print(f'Incorrect file format: {config_path}')
            sys.exit(-1)
        pass

    # print all channels info
    def show_groups(self) -> None:
        for group in self.groups:
            print(f'channel id: {group.id}')
            print(f'admins ids: {group.admins_ids}')
            print(f'users ids: {group.users_ids}')
            print(f'banned users ids: {group.banned_users_ids}')
            print('----------------------------------')
        pass

    # write to json
    @staticmethod
    def __write_json_file(path: str, data: dict) -> object:

        try:
            f = open(path, 'w')
        except IOError:
            print(f'Could not open json file: {path}')
            sys.exit(-1)

        try:
            json.dump(data, f, indent=4)
            f.close()
        except json.JSONDecodeError:
            print(f'Could not write json file: {path}')
            f.close()
            sys.exit(-1)

        return data

    # load from json
    @staticmethod
    def __load_json_file(path) -> object:

        data = {}

        try:
            f = open(path, 'r')
        except IOError:
            print(f'Could not open file: {path}')
            sys.exit(-1)

        try:
            data = json.load(f)
            f.close()
        except json.JSONDecodeError:
            print(f'Could not open file: {path}')
            f.close()
            sys.exit(-1)

        return data

    # write to file
    @staticmethod
    def __write_file(path: str, data: str) -> str:

        try:
            f = open(path, 'w')
        except IOError:
            print(f'Could not open file: {path}')
            sys.exit(-1)

        with f:
            try:
                data = f.write(data.__str__())
                f.close()
            except IOError:
                print(f'Could not write to file: {path}')
                f.close()
                sys.exit(-1)

        return str(data)

    # load from file as a string
    @staticmethod
    def __load_file(path) -> str:

        try:
            f = open(path, 'r')
        except IOError:
            print(f'Could not open file: {path}')
            sys.exit(-1)

        with f:
            try:
                data = f.read()
                f.close()
            except IOError:
                print(f'Could not read file: {path}')
                f.close()
                sys.exit(-1)

        return str(data)
