from telegram import Message, User


def send_not_permissions(message: Message, sender: User) -> None:
    message_text = f'{sender.name}, you do not have permissions'
    message.reply_text(message_text)
