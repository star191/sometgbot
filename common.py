from threading import Lock


class Singleton(type):
    """
    Thread-safe Singleton implement for Config class
    """

    _instances = {}
    _lock: Lock = Lock()

    def __call__(cls, *args, **kwargs):
        with cls._lock:
            if cls not in cls._instances:
                cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
            return cls._instances[cls]


def obj2dic(d):
    top = type('new', (object,), d)
    seqs = tuple, list, set, frozenset
    for i, j in d.items():
        if isinstance(j, dict):
            setattr(top, i, obj2dic(j))
        elif isinstance(j, seqs):
            setattr(top, i,
                    type(j)(obj2dic(sj) if isinstance(sj, dict) else sj for sj in j))
        else:
            setattr(top, i, j)
    return top


def load_json_file(path) -> (object, bool):
    data = {}

    try:
        f = open(path, 'r', encoding="UTF8")
    except IOError:
        print(f'Could not open file: {path}')
        return None, False

    import json
    try:
        data = json.load(f)
        f.close()
    except json.JSONDecodeError:
        print(f'Could not open file: {path}')
        f.close()
        return None, False

    return data, True
