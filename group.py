from typing import List


class Group:
    def __init__(self, data: dict = None):
        self.id = None
        self.admins_ids = []  # type: List[int]
        self.users_ids = []  # type: List[int]
        self.banned_users_ids = []  # type: List[int]

        if data is not None:
            self.id = data['id']
            self.admins_ids = data['admins_ids']
            self.users_ids = data['users_ids']
            self.banned_users_ids = data['banned_users_ids']
    pass


