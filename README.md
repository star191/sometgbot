# Launchpad

TG Bot


## Deps

- python3.6.8
- python-telegram-bot (Version: 12.2.0)


## Install

- install all deps
- (optional) use any of virtual env
- place config files in `config` folder (see `config/example_configs`)
- place `token` file with tg token in `config` folder
- run `python main.py`

## Denial of responsibility

- When I was writing this, I didn't know Python. <br>And I wasn't use best programming practices. <br>Be ready for bad code =)
- The TG API may have changed, so some functionality may not work.

