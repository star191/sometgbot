import bot
from commands.addowner.command_decorator import addowner_decorator
from commands.appeal.command_decorator import appeal_decorator
from commands.ban.command_decorator import ban_decorator
from commands.chatid.command import chatid
from commands.claim.command_decorator import claim_decorator
from commands.delowner.command_decorator import delowner_decorator
from commands.help.command_decorator import help_decorator
from commands.invite.command_decorator import invite_decorator
from commands.join.command_decorator import join_decorator
from commands.meow.command import meow
from commands.setlink.command_decorator import setlink_decorator
from commands.setlogginggroup.command_decorator import setlogginggroup_decorator
from commands.setmaingroup.command_decorator import setmaingroup_decorator
from commands.start.command_decorator import start_decorator
from commands.test.command import test_decorator, test
from commands.userid.command import userid
from middlewares.wares.users_logger import get_users_logger_middleware

config_path = './config/'
launchpad = bot.Launchpad(config_path)

launchpad.add_command_handler('test', test)
launchpad.add_command_decorator('dtest', test_decorator)
launchpad.add_command_decorator('start', start_decorator)
launchpad.add_command_handler('meow', meow)
launchpad.add_command_handler('userid', userid)
launchpad.add_command_handler('chatid', chatid)
launchpad.add_command_decorator('addowner', addowner_decorator)
launchpad.add_command_decorator('delowner', delowner_decorator)
launchpad.add_command_decorator('ban', ban_decorator)
launchpad.add_command_decorator('claim', claim_decorator)
launchpad.add_command_decorator('appeal', appeal_decorator)
launchpad.add_command_decorator('invite', invite_decorator)
launchpad.add_command_decorator('setlink', setlink_decorator)
launchpad.add_command_decorator('setgroup', setmaingroup_decorator)
launchpad.add_command_decorator('setlogchat', setlogginggroup_decorator)
launchpad.add_command_decorator('join', join_decorator)
launchpad.add_command_decorator('help', help_decorator)
launchpad.add_message_middleware(get_users_logger_middleware())


launchpad.start()



