from telegram import Update
from telegram.ext import CallbackContext

from bot import Launchpad


# for fetch data to db
def main_message_handler_decorate(launchpad: Launchpad):

    def main_message_handler(update: Update, context: CallbackContext):
        print('main_message_handler()')

        middleware_handler(launchpad, update, context)

    return main_message_handler


def middleware_handler(launchpad: Launchpad, update: Update, context: CallbackContext):
    launchpad.messageMiddlewareManager.start(update, context)

    pass

