from typing import List

import telegram
from telegram import Update, User, Message
from telegram.ext import CallbackContext

import middlewares
from bot import Launchpad
from resources import getLocalString, Strings

NEW_CHAT_MEMBER = 'new_chat_members'
LEFT_CHAT_MEMBER = 'left_chat_member'


def get_users_logger_middleware() -> middlewares.MessageMiddleware:
    ware = middlewares.MessageMiddleware(callback=callback, m_type=middlewares.INFINITY)

    return ware


def callback(launchpad: Launchpad, update: Update, context: CallbackContext) -> bool:
    message: Message
    sender: User

    message = update.message

    if message.from_user is not None:
        sender = message.from_user
    else:
        print('sender is None')
        return False

    if not is_main_group(launchpad, message):
        print('This is not the Main Group')
        return False

    res, ok = is_member_update(message)
    if not ok:
        return False

    print('users_logger_middleware()')

    send_to_logging_group(launchpad, message, context, res)

    return True


def is_member_update(msg: Message) -> (str, bool):

    if msg.new_chat_members is not None and len(msg.new_chat_members) != 0:
        return NEW_CHAT_MEMBER, True

    if msg.left_chat_member is not None:
        return LEFT_CHAT_MEMBER, True

    return None, False


def is_main_group(launchpad: Launchpad, msg: Message) -> bool:

    if launchpad.config.main_group['id'] == msg.chat_id:
        return True

    return False


def send_to_logging_group(launchpad: Launchpad, msg: Message, ctx: CallbackContext, res: str):
    msg_text = None

    if res == NEW_CHAT_MEMBER:
        pre_msg_text = getLocalString(Strings.users_logger_New_chat_members)
        new_members = msg.new_chat_members  # type: List[User]
        msg_text = '\n'.join([user.name for user in new_members])
        msg_text = f'{pre_msg_text}\n{msg_text}'
        pass

    if res == LEFT_CHAT_MEMBER:
        pre_msg_text = getLocalString(Strings.users_logger_Left_chat_member)
        left_member = msg.left_chat_member  # type: User
        msg_text = f'{left_member.name}'
        msg_text = f'{pre_msg_text}\n{msg_text}'
        pass

    logging_group_id = launchpad.config.logging_group['id']

    try:
        ctx.bot.send_message(chat_id=logging_group_id, text=msg_text)
    except telegram.TelegramError as err:
        print(err.message)

    pass
