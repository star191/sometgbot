from telegram import Update
from telegram.ext import CallbackContext

# types
INFINITY = "infinity"
ONCE = 'once'
REPEATEDLY = 'repeatedly'


class WrongTypeError(Exception):
    def __init__(self, code):
        self.code = code

    def __str__(self):
        return repr(self.code)
    pass


class WrongCounts(Exception):
    def __init__(self, code):
        self.code = code

    def __str__(self):
        return repr(self.code)
    pass


class Middleware:
    # type of middleware
    m_type: str
    # number of repetitions
    _counts: int
    # callback function
    callback = None

    def __init__(self, callback, m_type: str = INFINITY):
        self.callback = callback
        self.m_type = m_type
        self._counts = 0
        pass

    def getCounts(self) -> int:
        return self._counts

    def setCounts(self, counts: int):
        if self.m_type != REPEATEDLY:
            raise WrongTypeError('MiddleWare type is not REPEATEDLY')
        if counts <= 0:
            raise WrongCounts('Counts must be greater than 0')
        self._counts = counts
        pass

    def decrementCounts(self):
        if self.m_type != REPEATEDLY:
            raise WrongTypeError('MiddleWare type is not REPEATEDLY')
        if self._counts <= 0:
            raise WrongCounts('Counts must be greater than 0')
        self._counts = self._counts - 1
        pass

    pass


class MessageMiddleware(Middleware):
    """
        callback: func(launchpad: Launchpad, update: Update, context: CallbackContext) -> bool
    """

    def run(self, launchpad, update: Update, context: CallbackContext) -> bool:
        return self.callback(launchpad, update, context)

    pass


# works with MessageMiddlewareManager
class ConversationMiddleware(MessageMiddleware):
    """
    callback: func(launchpad: Launchpad, update: Update, context: CallbackContext) -> bool
    """

    def __init__(self, conversation_user_id, callback):
        super(ConversationMiddleware, self).__init__(callback=callback, m_type=ONCE)
        self.conversation_user_id = conversation_user_id
        pass

    def run(self, launchpad, update: Update, context: CallbackContext) -> bool:
        return self.__run(launchpad, update, context)
        pass

    def __run(self, launchpad, update: Update, context: CallbackContext) -> bool:
        message = update.message
        sender = message.from_user
        if sender is None:
            print('sender is None')
            return False
        
        # Not in private chat
        if message.chat.type != message.chat.PRIVATE:
            print('Chat type is not private')
            # send_not_in_private_chat(message)
            return False

        # is the same user
        if self.conversation_user_id == sender.id:
            # callback return True if so all right, else it will repeat at next start
            if self.callback(launchpad, update, context):
                return True

        return False

    pass

