import unittest

from telegram import Update, User
from telegram.ext import CallbackContext, Updater

from middlewares import REPEATEDLY, MessageMiddleware, INFINITY, ONCE, ConversationMiddleware
from middlewares.mwmanagaer import MessageMiddlewareManager


class Message:
    from_user: None
    pass


class TestMessageMiddleware(unittest.TestCase):

    def test_setCounts(self):
        def callback(launchpad, update, context):
            pass

        counts = 5
        m_type = REPEATEDLY
        mm = MessageMiddleware(callback=callback, m_type=m_type)

        mm.setCounts(counts)

        self.assertEqual(first=mm._counts, second=counts)
        self.assertEqual(first=mm.getCounts(), second=counts)

        pass

    def test_decrementCounts(self):
        def callback(launchpad, update, context):
            pass

        counts = 7
        m_type = REPEATEDLY
        mm = MessageMiddleware(callback=callback, m_type=m_type)
        mm.setCounts(counts)

        mm.decrementCounts()
        self.assertEqual(first=mm.getCounts(), second=counts-1)
        mm.decrementCounts()
        self.assertEqual(first=mm.getCounts(), second=counts - 2)
        mm.decrementCounts()
        self.assertEqual(first=mm.getCounts(), second=counts - 3)

        pass

    def test_run(self):
        data = {
            'ok': False
        }

        def callback(launchpad, update, context):
            data['ok'] = True
            pass

        update = Update(0)
        updater = Updater('000000000:AAAA_BBBBBBBBBBBBBBBBBBBBBBBBBBBBBB', use_context=True)
        context = CallbackContext(updater.dispatcher)

        mm = MessageMiddleware(callback=callback)
        mm.run(None, update, context)

        self.assertTrue(data['ok'])

        pass

    pass


class TestMessageMiddlewareManager(unittest.TestCase):

    def test_add_ware(self):
        def callback(launchpad, update, context):
            pass

        ware1 = MessageMiddleware(callback=callback)
        ware2 = MessageMiddleware(callback=callback)

        mmm = MessageMiddlewareManager(None)
        mmm.add_ware(ware1)
        mmm.add_ware(ware2)

        self.assertEqual(len(mmm.queue), 2)
        for ware in mmm.queue:
            self.assertTrue(isinstance(ware, MessageMiddleware))

        pass

    def test_del_ware(self):
        def callback(launchpad, update, context):
            pass

        ware1 = MessageMiddleware(callback=callback)
        ware2 = MessageMiddleware(callback=callback)

        mmm = MessageMiddlewareManager(None)
        mmm.add_ware(ware1)
        mmm.add_ware(ware2)

        mmm.del_ware(ware1)
        self.assertEqual(len(mmm.queue), 1)
        mmm.del_ware(ware2)
        self.assertEqual(len(mmm.queue), 0)

        pass

    def test_type_operations(self):
        def callback(launchpad, update, context):
            pass

        ware1 = MessageMiddleware(callback=callback, m_type=INFINITY)
        ware2 = MessageMiddleware(callback=callback, m_type=REPEATEDLY)
        ware3 = MessageMiddleware(callback=callback, m_type=ONCE)

        ware2.setCounts(3)

        mmm = MessageMiddlewareManager(None)
        mmm.add_ware(ware1)
        mmm.add_ware(ware2)
        mmm.add_ware(ware3)

        self.assertEqual(len(mmm.queue), 3)

        mmm.type_operations(ware1)
        mmm.type_operations(ware2)
        mmm.type_operations(ware3)
        self.assertEqual(len(mmm.queue), 2)

        mmm.type_operations(ware1)
        mmm.type_operations(ware2)
        mmm.type_operations(ware3)
        self.assertEqual(len(mmm.queue), 2)

        mmm.type_operations(ware1)
        mmm.type_operations(ware2)
        mmm.type_operations(ware3)
        self.assertEqual(len(mmm.queue), 1)

        pass

    def test_start(self):

        data = {
            'ware1': False,
            'ware2': False,
            'ware3': False,
        }

        def callback1(launchpad, update, context):
            data['ware1'] = True
            pass

        def callback2(launchpad, update, context):
            data['ware2'] = True
            pass

        def callback3(launchpad, update, context):
            data['ware3'] = True
            pass

        update = Update(0)
        updater = Updater('000000000:AAAA_BBBBBBBBBBBBBBBBBBBBBBBBBBBBBB', use_context=True)
        context = CallbackContext(updater.dispatcher)

        ware1 = MessageMiddleware(callback=callback1, m_type=INFINITY)
        ware2 = MessageMiddleware(callback=callback2, m_type=REPEATEDLY)
        ware3 = MessageMiddleware(callback=callback3, m_type=ONCE)

        ware2.setCounts(3)

        mmm = MessageMiddlewareManager(None)
        mmm.add_ware(ware1)
        mmm.add_ware(ware2)
        mmm.add_ware(ware3)

        mmm.start(update, context)

        self.assertTrue(data['ware1'])
        self.assertTrue(data['ware2'])
        self.assertTrue(data['ware3'])

        self.assertEqual(len(mmm.queue), 2)

        data['ware1'] = False
        data['ware2'] = False

        mmm.start(update, context)
        self.assertTrue(data['ware1'])
        self.assertTrue(data['ware2'])

        self.assertEqual(len(mmm.queue), 2)

        data['ware1'] = False
        data['ware2'] = False

        mmm.start(update, context)
        self.assertTrue(data['ware1'])
        self.assertTrue(data['ware2'])
        self.assertEqual(len(mmm.queue), 1)

        pass

    pass


class TestConversationMiddleware(unittest.TestCase):

    def setUp(self) -> None:
        self.update = Update(0)
        self.updater = Updater('000000000:AAAA_BBBBBBBBBBBBBBBBBBBBBBBBBBBBBB', use_context=True)
        self.context = CallbackContext(self.updater.dispatcher)
        self.data = {
            'param1': False,
            'param2': False,
            'param3': False,
        }

        pass

    def test_init(self):

        user_id = '42'

        def callback(launchpad, update: Update, context: CallbackContext):
            pass

        ware = ConversationMiddleware(conversation_user_id=user_id, callback=callback)

        self.assertEqual(ONCE, ware.m_type)
        self.assertEqual(user_id, ware.conversation_user_id)
        self.assertEqual(callback, ware.callback)

        pass

    def test_run(self):
        user_id = 42
        launchpad = None
        result = False

        self.update.message = Message()
        self.update.message.from_user = None

        def callback(launchpad, update: Update, context: CallbackContext) -> bool:
            self.data['param1'] = True
            self.data['param3'] = True
            return result
            pass

        ware = ConversationMiddleware(conversation_user_id=user_id, callback=callback)

        res = ware.run(launchpad, self.update, self.context)

        # sender is None
        self.assertFalse(res)
        self.assertEqual(self.data['param1'], False)
        self.assertEqual(self.data['param3'], False)

        # there is not the same user
        another_id = 100500
        self.update.message.from_user = User(id=another_id, first_name="", is_bot=False)

        res = ware.run(launchpad, self.update, self.context)
        self.assertFalse(res)
        self.assertEqual(self.data['param1'], False)
        self.assertEqual(self.data['param3'], False)

        # there is the same user, result = False
        self.update.message.from_user = User(id=user_id, first_name="", is_bot=False)
        self.assertEqual(user_id, self.update.message.from_user.id)

        res = ware.run(launchpad, self.update, self.context)
        self.assertFalse(res)
        self.assertEqual(self.data['param1'], True)
        self.assertEqual(self.data['param3'], True)

        # result = True
        result = True
        res = ware.run(launchpad, self.update, self.context)
        self.assertTrue(res)
        self.assertEqual(self.data['param1'], True)
        self.assertEqual(self.data['param3'], True)


        pass

    pass


class TestMessageMiddlewareManagerWithConversationMiddleware(unittest.TestCase):

    def setUp(self) -> None:
        self.update = Update(0)
        self.updater = Updater('000000000:AAAA_BBBBBBBBBBBBBBBBBBBBBBBBBBBBBB', use_context=True)
        self.context = CallbackContext(self.updater.dispatcher)
        self.update.message = Message()
        self.update.message.from_user = None

        self.data = {
            'param1': False,
            'param2': False,
            'param3': False,
        }

        pass

    def test_start(self):

        def callback1(launchpad, update, context) -> bool:
            self.data['param1'] = True
            return True
            pass

        def callback2(launchpad, update, context) -> bool:
            self.data['param2'] = True
            return True
            pass

        def callback3(launchpad, update, context) -> bool:
            self.data['param3'] = True
            return True
            pass

        user_id_1 = 1
        user_id_2 = 2
        user_id_3 = 3

        ware1 = ConversationMiddleware(user_id_1, callback=callback1)
        ware2 = ConversationMiddleware(user_id_2, callback=callback2)
        ware3 = ConversationMiddleware(user_id_3, callback=callback3)

        mmm = MessageMiddlewareManager(None)
        mmm.add_ware(ware1)
        mmm.add_ware(ware2)
        mmm.add_ware(ware3)

        # sender is None
        mmm.start(self.update, self.context)
        self.assertTrue(ware1 in mmm.queue)
        self.assertTrue(ware2 in mmm.queue)
        self.assertTrue(ware3 in mmm.queue)
        self.assertEqual(3, len(mmm.queue))

        self.assertEqual(self.data['param1'], False)
        self.assertEqual(self.data['param2'], False)
        self.assertEqual(self.data['param3'], False)

        # user_id = user_id_2
        sender = self.update.message.from_user = User(id=user_id_2, first_name="", is_bot=False)

        self.assertEqual(user_id_2, self.update.message.from_user.id)

        mmm.start(self.update, self.context)
        self.assertTrue(ware1 in mmm.queue)
        self.assertTrue(ware2 not in mmm.queue)
        self.assertTrue(ware3 in mmm.queue)
        self.assertEqual(2, len(mmm.queue))

        self.assertEqual(self.data['param1'], False)
        self.assertEqual(self.data['param2'], True)
        self.assertEqual(self.data['param3'], False)

        # user_id = user_id_3
        sender.id = user_id_3
        self.assertEqual(user_id_3, self.update.message.from_user.id)

        mmm.start(self.update, self.context)
        self.assertTrue(ware1 in mmm.queue)
        self.assertTrue(ware2 not in mmm.queue)
        self.assertTrue(ware3 not in mmm.queue)
        self.assertEqual(1, len(mmm.queue))

        self.assertEqual(self.data['param1'], False)
        self.assertEqual(self.data['param2'], True)
        self.assertEqual(self.data['param3'], True)

        # user_id = user_id_2
        sender.id = user_id_2
        self.assertEqual(user_id_2, self.update.message.from_user.id)

        mmm.start(self.update, self.context)
        self.assertTrue(ware1 in mmm.queue)
        self.assertTrue(ware2 not in mmm.queue)
        self.assertTrue(ware3 not in mmm.queue)
        self.assertEqual(1, len(mmm.queue))

        self.assertEqual(self.data['param1'], False)
        self.assertEqual(self.data['param2'], True)
        self.assertEqual(self.data['param3'], True)

        # user_id = user_id_1
        sender.id = user_id_1
        self.assertEqual(user_id_1, self.update.message.from_user.id)

        mmm.start(self.update, self.context)
        self.assertTrue(ware1 not in mmm.queue)
        self.assertTrue(ware2 not in mmm.queue)
        self.assertTrue(ware3 not in mmm.queue)
        self.assertEqual(0, len(mmm.queue))

        self.assertEqual(self.data['param1'], True)
        self.assertEqual(self.data['param2'], True)
        self.assertEqual(self.data['param3'], True)

        pass

    pass
