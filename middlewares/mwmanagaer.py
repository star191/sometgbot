from telegram import Update
from telegram.ext import CallbackContext

from middlewares import REPEATEDLY, ONCE

"""
TODO:
In future:
    automated message_handlers deleting 
        - create message_handler by Filters in middleware
        - add middleware to pool joined message_handler by Filters
        - delete message_handler if middleware pool is empty    
"""


class MiddlewareManager:
    queue: []
    launchpad: None

    def __init__(self, launchpad):
        self.queue = []
        self.launchpad = launchpad
        pass

    def add_ware(self, middleware):
        self.queue.append(middleware)
        pass

    def del_ware(self, middleware) -> bool:
        if middleware in self.queue:
            self.queue.remove(middleware)
            return True
        else:
            return False
        pass

    def type_operations(self, ware):
        if ware.m_type == REPEATEDLY:
            ware.decrementCounts()
            if ware.getCounts() <= 0:
                self.del_ware(ware)

        if ware.m_type == ONCE:
            self.del_ware(ware)

        pass
    pass


class MessageMiddlewareManager(MiddlewareManager):

    # callback for MessageHandler
    def start(self, update: Update, context: CallbackContext):
        for ware in self.queue:

            if ware.run(self.launchpad, update, context):
                self.type_operations(ware)

        pass

    pass

