import sys

import locales
from common import obj2dic, load_json_file

__strings_path = './resources/strings.json'


def _loadResourcesStrings(path: str):

    data, ok = load_json_file(path)

    if not ok:
        print('Cannot load resource strings')
        sys.exit(-1)

    obj = obj2dic(data)

    return obj


def getLocalString(string: object) -> str:
    loc = __getLocale()

    if loc in string.__dict__:
        return string.__dict__.get(__getLocale())

    print('Unknown locale')
    return string.default
    pass


def __getLocale() -> str:
    locale = locales.GetLocale()
    return locale


# exports
Strings = _loadResourcesStrings(__strings_path)
