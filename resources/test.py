import unittest

import resources


class ResourcesTest(unittest.TestCase):

    def setUp(self) -> None:
        self.strings_path = "./test_strings.json"
        pass

    def test_getResourceStrings(self):

        obj = resources._loadResourcesStrings(self.strings_path)

        self.assertIsNotNone(obj.string1)

        self.assertIsNotNone(obj.string1.default)
        print(obj.string1.default)

        pass

    def test_getLocalString(self):

        obj = resources._loadResourcesStrings(self.strings_path)

        self.assertIsNotNone(resources.getLocalString(obj.string1))
        print(resources.getLocalString(obj.string1))

        self.assertIsNotNone(resources.getLocalString(obj.string2))
        print(resources.getLocalString(obj.string2))

        pass

    def test_unknown_locale_getLocatSrting(self):

        import locales

        locales.SetLocale(locales.Locales.de_DE)

        obj = resources._loadResourcesStrings(self.strings_path)

        self.assertIsNotNone(resources.getLocalString(obj.string1))
        print(resources.getLocalString(obj.string1))

        self.assertIsNotNone(resources.getLocalString(obj.string2))
        print(resources.getLocalString(obj.string2))

        pass
