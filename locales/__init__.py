
class Locales:
    DEFAULT = 'default'
    en_US = 'en_US'
    de_DE = 'de_DE'
    fr_FR = 'fr_FR'
    ru_RU = 'ru_RU'
    pass


class __Locale:

    def __init__(self):
        self.__locale = ''
        pass

    def getLocale(self) -> str:
        if self.__locale is None or len(self.__locale) == 0:
            print("Locale not set")
            return Locales.DEFAULT
        return self.__locale

    def setLocale(self, locale) -> bool:

        ok = False
        for loc in Locales.__dict__.values():
            if locale == loc:
                ok = True
                break
            pass

        if ok:
            self.__locale = locale
            return True
        else:
            print("Unknown locale")
            return False

        pass

    pass


__locale = __Locale()


def GetLocale() -> str:
    return __locale.getLocale()
    pass


def SetLocale(locale) -> bool:
    return __locale.setLocale(locale)
    pass
