import datetime
import logging
from typing import List

import telegram
from telegram.ext import (
    Updater,
    Dispatcher,
    CommandHandler,
    MessageHandler, ConversationHandler, Filters)

from config import cfg
from group import Group
from middlewares import MessageMiddleware
from middlewares.mwmanagaer import MessageMiddlewareManager


class Launchpad:
    config: cfg.Config
    bot_id: int
    updater: Updater
    dispatcher: Dispatcher
    messageMiddlewareManager: MessageMiddlewareManager
    logger: logging.Logger
    owners: List[int]
    groups: List[Group]

    def __init__(self, config_path):
        logging.basicConfig(level=logging.INFO,
                            format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.DEBUG)

        self.logger.info('Initialization...')

        self.config = cfg.Config(config_path)

        self.logger.info(f'token: [{self.config.TOKEN}]')

        self.updater = Updater(token=self.config.TOKEN, use_context=True)

        self.dispatcher = self.updater.dispatcher

        self.bot_id = self.updater.bot.id
        self.owners = self.config.bot_owners_ids
        self.groups = self.config.groups

        self.init_middlewares()

    def start(self) -> None:
        self.updater.start_polling()
        self.logger.info('Running...')
        self.updater.idle()

    def stop(self) -> None:
        self.config.save_config()
        self.updater.stop()

    def init_middlewares(self):
        self.messageMiddlewareManager = MessageMiddlewareManager(self)

        # register the MessageMiddlewareManager
        mh = MessageHandler(Filters.text | Filters.photo | Filters.video | Filters.document | Filters.status_update.new_chat_members | Filters.status_update.left_chat_member,
                            self.messageMiddlewareManager.start)
        self.dispatcher.add_handler(mh)

        pass

    def add_command_handler(self, command_name, callback) -> None:
        h = CommandHandler(command_name, callback)
        self.dispatcher.add_handler(h)

    def add_command_handler_args(self, command_name, callback) -> None:
        h = CommandHandler(command_name, callback, pass_args=True)
        self.dispatcher.add_handler(h)

    def add_message_handler(self, callback, filters=None) -> None:
        mh = MessageHandler(filters, callback)
        self.dispatcher.add_handler(mh)

    def add_error_handler(self, callback) -> None:
        self.dispatcher.add_error_handler(callback)

    def add_command_decorator(self, command_name, decorator) -> None:
        callback = decorator(self)
        h = CommandHandler(command_name, callback)
        self.dispatcher.add_handler(h)

    def add_message_middleware(self, middleware: MessageMiddleware):
        self.messageMiddlewareManager.add_ware(middleware)
        pass

    def add_conversation_handler_decorator(self, decorator):
        handler: ConversationHandler = decorator(self)
        self.dispatcher.add_handler(handler)
        pass

    def add_owner(self, user_id: int) -> (bool, str):
        print('add_owner()')
        if user_id in self.owners:
            return False, 'exist'

        self.owners.append(user_id)
        self.config.save_config()
        return True, ''

    def del_owner(self, user_id) -> (bool, str):
        if user_id not in self.owners:
            return False, 'not_exist'
        else:
            self.owners.remove(user_id)
            self.config.save_config()

        return True, ''

    def __kick_user(self, chat_id, user_id):
        self.updater.bot.kick_chat_member(chat_id, user_id)
        pass

    def local_ban(self, chat_id, user_id, debug_not_kick: bool = None) -> (bool, str):
        # add chat to groups if chat not exists
        # just kick the user
        # add user to banned list to group
        self.__add_chat_if_not_exists_in_groups(chat_id)
        ok = self.__add_user_to_ban_list(chat_id, user_id)
        if not ok:
            return False, 'not_added_to_ban_list'
        if not debug_not_kick:
            self.__kick_user(chat_id, user_id)

        self.config.save_groups()
        return True, ''
        pass

    def __add_user_to_ban_list(self, chat_id, user_id) -> bool:
        for group in self.groups:
            if chat_id == group.id:
                group.banned_users_ids.append(user_id)
                return True

        return False
        pass

    def __add_chat_if_not_exists_in_groups(self, chat_id):
        for group in self.groups:
            if group.id == chat_id:
                return
        group = Group()
        group.id = chat_id
        self.groups.append(group)
        pass

    def check_permission(self, user_id) -> bool:
        if len(self.owners) == 0:
            return True
        else:
            if user_id in self.owners:
                return True
        return False

    def is_My_Master(self, user_id) -> bool:
        if user_id in self.owners:
            return True
        else:
            return False

    pass

    def get_claim_datetime(self, claimer_id: int, user_id: int) -> (None or datetime.datetime):
        print('get_claim_datetime()')
        claim_time = None

        for claim in self.config.claims:
            if claimer_id == claim['claimer_id'] and user_id == claim['user_id']:
                timestamp = claim['timestamp']
                claim_time = datetime.datetime.fromtimestamp(timestamp)
                break

        return claim_time

    def get_appeal_datetime(self, user_id: int) -> (None or datetime.datetime):
        print('get_appeal_datetime()')
        appeal_time = None

        for appeal in self.config.appeals:
            if user_id == appeal['user_id']:
                timestamp = appeal['timestamp']
                appeal_time = datetime.datetime.fromtimestamp(timestamp)
                break

        return appeal_time

    def add_claim(self, claimer_id: int, user_id: int):

        timestamp = datetime.datetime.now().timestamp()

        for clm in self.config.claims:
            if claimer_id == clm['claimer_id'] and user_id == clm['user_id']:
                clm['timestamp'] = timestamp
                return

        claim = {
          "claimer_id": claimer_id,
          "user_id": user_id,
          "timestamp": timestamp
        }
        self.config.claims.append(claim)
        pass

    def add_appeal(self, user_id: int):

        timestamp = datetime.datetime.now().timestamp()

        for app in self.config.appeals:
            if user_id == app['user_id']:
                app['timestamp'] = timestamp
                return

        appeal = {
            "user_id": user_id,
            "timestamp": timestamp
        }
        self.config.appeals.append(appeal)
        pass

    def set_invite_link(self, link):
        self.config.set_invite_link(link)
        pass

    def get_invite_link_to_group(self):
        link = self.config.invite_link
        return link
        pass

    def set_main_group(self, chat_id) -> bool:

        try:
            self.dispatcher.bot.getChat(chat_id)
        except telegram.TelegramError:
            return False

        main_group = {
            'id': chat_id,
            'users_ids': [],
            'banned_users_ids': [],
            'history_users_ids': []
        }

        self.config.update_main_group(main_group)

        return True
        pass

    def set_logging_group(self, chat_id) -> bool:

        try:
            self.dispatcher.bot.getChat(chat_id)
        except telegram.TelegramError:
            return False

        logging_group = {
            'id': chat_id
        }

        self.config.update_logging_group(logging_group)

        return True
        pass
